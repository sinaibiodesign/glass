### What is Glass Stream? ###

Glass Stream provides an environment to remotely record and stream Google Glass video over a closed network. It includes an Android application, Google Glass application, and a python server.

### Requirements ###

* Wowza Streaming Engine
* Android device for running the Console application (SDK version >= 19).
* Any Google Glass device
* Wireless network on which both Android and Glass devices reside
* Accessible server running MySQL and Wowza Streaming Engine

### Installation and Use ###

* MySQL table is included in the source. Read in provided MySQL dump file into the local server's MySQL DB, creating the "glassstream" database.
* Install the Android application (Console) on your Android Device.
* Install the Glass app on your Google Glass.
* Run the python server on the local machine like:

```
python server.pyc
```

* Run the Glass app on the Google Glass by speaking "OK Glass, Record A Video, Glass Stream"
* Select the IP of the local machine on the Glass app.
* Select the name of the recorder.
* Run the Console app on your Android device.
* Select the local server IP, Glass recorder, resolution, and procedure. Begin recording by touching the start button.
* Stream can be viewed in real time by touching the play button.
* Specific procedures can be annotated by clicking the corresponding button on the Console app.

### Credits ###

* Uses freely available libstreaming library at https://github.com/fyhertz/libstreaming.

### Citation ###

Publications or software making substantial use of Glass Stream or its derivatives should cite this software package and freely available online repository. An example citation is given as:

    P. Melana-Dayton, A. B. Costa, "Glass Stream", Package Version <Insert Version Number>,
    http://bitbucket.org/sinaineurosurgery/glass

### Contact ###

Anthony B. Costa, anthony.costa@mssm.edu