package org.mountsinai.glassstream;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.SurfaceHolder;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.surgery.glassstream.R;

import net.majorkernelpanic.streaming.MediaStream;
import net.majorkernelpanic.streaming.Session;
import net.majorkernelpanic.streaming.SessionBuilder;
import net.majorkernelpanic.streaming.audio.AudioQuality;
import net.majorkernelpanic.streaming.rtsp.RtspClient;
import net.majorkernelpanic.streaming.gl.SurfaceView;
import net.majorkernelpanic.streaming.video.VideoQuality;

public class MainActivity extends Activity implements RtspClient.Callback, Session.Callback, SurfaceHolder.Callback {

    private String path = "";
    private String ip = "";
    private int x;
    private int y;
    private int bitrate;
    private Session mSession;
    private RtspClient mClient;

    private Socket socket = null;
    private PrintWriter out;
    private BufferedReader in;
    private Thread background;

    private final Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            // get the bundle and extract data by key
            Bundle b = msg.getData();
            String key = b.getString("Action");
            if (key.startsWith("TOGGLE")) {
                String options = key.split(";")[1];
                String[] opts = options.split("\\|");
                String[] res = opts[0].split("x");
                x = Integer.parseInt(res[0]);
                y = Integer.parseInt(res[1]);
                bitrate = Integer.parseInt(opts[1]);

                toggleStream();
            }
            return true;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        SurfaceView mSurfaceView = (SurfaceView) findViewById(R.id.surface);

        // Configures the SessionBuilder
        mSession = SessionBuilder.getInstance()
                .setContext(getApplicationContext())
                .setAudioEncoder(SessionBuilder.AUDIO_AAC)
                .setAudioQuality(new AudioQuality(8000, 16000))
                .setVideoEncoder(SessionBuilder.VIDEO_H264)
                .setSurfaceView(mSurfaceView)
                .setCamera(Camera.CameraInfo.CAMERA_FACING_FRONT)
                .setPreviewOrientation(0)
                .setCallback(this)
                .build();

        mSession.getVideoTrack().setStreamingMethod(MediaStream.MODE_MEDIACODEC_API);

        // Configures the RTSP client
        mClient = new RtspClient();
        mClient.setSession(mSession);
        mClient.setCallback(this);



        mSurfaceView.getHolder().addCallback(this);

        new IPChooser().execute();
    }

    protected void onStart() {
        super.onStart();

        background = new Thread(new Runnable() {

            @Override
            public void run() {
                String msg;
                while (true) {
                    try {
                        msg = in.readLine();
                        //if (msg!=null) {
                        Message msgObj = handler.obtainMessage();
                        Bundle b = new Bundle();
                        b.putString("Action", msg);
                        msgObj.setData(b);
                        handler.sendMessage(msgObj);
                        //}
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        break;
                    }
                }

            }

        }
        );

    }


    private void selectStream(String str) {
        Intent i = new Intent(this, ChooseActivity.class);
        i.putExtra("EXTRA", str);
        startActivityForResult(i, 1);
    }

    private void selectIP(String str)
    {
        Intent i = new Intent(this, ChooseActivity.class);
        i.putExtra("EXTRA", str);
        startActivityForResult(i, 2);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String choice = (data.getStringExtra("EXTRA"));
        Toast toast = Toast.makeText(this, choice, Toast.LENGTH_LONG);
        toast.show();
        if(requestCode==1)
            new sendChoice().execute(choice);
        else if(requestCode==2) {
            ip = choice;
            new Good().execute();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mClient.release();
        mSession.release();
        try {
            in.close();
            socket.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        out.close();
    }

    private ArrayList<String> getIPs() {
        String subnet = "192.168.0";
        ArrayList<String> out = new ArrayList<String>();
        int timeout = 25;
        for (int i = 1; i < 255; i++) {
            String host = subnet + "." + i;
            try {
                if (InetAddress.getByName(host).isReachable(timeout)) {
                    out.add(host);
                }
            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return out;
    }

    // Connects/disconnects to the RTSP server and starts/stops the stream
    private void toggleStream() {
        if (!mClient.isStreaming()) {
            String port;
            mSession.setVideoQuality(new VideoQuality(x, y, 30, bitrate * 1000));

            port = "1935";

            String username = "storage";
            String password = "ferrari";
            mClient.setCredentials(username, password);
            mClient.setServerAddress(ip, Integer.parseInt(port));
            mClient.setStreamPath(path);
            mClient.startStream();

        } else {
            // Stops the stream and disconnects from the RTSP server
            mClient.stopStream();
        }
    }

    private void logError(final String msg) {
        @SuppressWarnings("unused")
        final String error = (msg == null) ? "Error unknown" : msg;
        // Displays a popup to report the error to the user
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

//	public void onClick(View v)
//	{
//		toggleStream();
//	}

    @Override
    public void onBitrateUpdate(long bitrate) {
    }

    @Override
    public void onSessionConfigured() {

    }

    @Override
    public void onSessionStarted() {
        Toast toast = Toast.makeText(this, "Streaming...", Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void onSessionStopped() {

    }

    @Override
    public void onSessionError(int reason, int streamType, Exception e) {
        switch (reason) {
            case Session.ERROR_CAMERA_ALREADY_IN_USE:
                break;
            case Session.ERROR_CAMERA_HAS_NO_FLASH:
                break;
            case Session.ERROR_INVALID_SURFACE:
                break;
            case Session.ERROR_STORAGE_NOT_READY:
                break;
            case Session.ERROR_CONFIGURATION_NOT_SUPPORTED:
                break;
            case Session.ERROR_OTHER:
                break;
        }

        if (e != null) {
            logError(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onRtspUpdate(int message, Exception e) {
        switch (message) {
            case RtspClient.ERROR_CONNECTION_FAILED:
            case RtspClient.ERROR_WRONG_CREDENTIALS:
                logError(e.getMessage());
                e.printStackTrace();
                break;
        }
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mSession.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mClient.stopStream();
    }

    @Override
    public void onPreviewStarted() {
    }

    private class sendChoice extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            out.println(params[0]);
            String link = "";
            try {
                link = in.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (link != "") path = link;
            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {

            background.start();

        }
    }


    private class IPChooser extends AsyncTask<Void, Void, String> {

        ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mDialog = new ProgressDialog(MainActivity.this);
            mDialog.setMessage("Loading IPs...");
            mDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            ArrayList<String> ips = getIPs();
            String out = "";
            for(String ipaddr:ips) {
                out += ipaddr + ",";
            }
            return out;
        }

        @Override
        protected void onPostExecute(String param) {
            selectIP(param);
            mDialog.dismiss();
        }

    }

    private class Good extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            if (socket == null) {
                try {
                    socket = new Socket(ip, 9096);
                    out = new PrintWriter(socket.getOutputStream(), true);
                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                } catch (UnknownHostException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }


            try {
                out.println("GLASS");
                String str = in.readLine();
                if (str != null) {
                    publishProgress(str);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onProgressUpdate(String... strings) {
            selectStream(strings[0]);
        }

    }


}