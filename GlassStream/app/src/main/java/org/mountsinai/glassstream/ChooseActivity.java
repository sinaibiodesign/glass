package org.mountsinai.glassstream;

import java.util.ArrayList;

import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.surgery.glassstream.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class ChooseActivity extends Activity {

    private RadioGroup mRadioGroup;
    private GestureDetector mGestureDetector;
    private Button mButton;
    private ArrayList<RadioButton> radioButtons;
    private int counter = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
        radioButtons = new ArrayList<RadioButton>();
        Intent intent = getIntent();
        String c = intent.getStringExtra("EXTRA");
        mRadioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        String[] choices = c.split(",");
        for(int i=0;i<choices.length;i++)
        {
            RadioButton rdbtn = new RadioButton(this);
            rdbtn.setText(choices[i]);
            rdbtn.setId(i);
            mRadioGroup.addView(rdbtn);
            radioButtons.add(rdbtn);
            rdbtn.setFocusableInTouchMode(true);
        }


    }

    @Override
    public void onStart()
    {
        super.onStart();

        mGestureDetector = createGestureDetector(this);
        mButton = (Button) findViewById(R.id.button);
        mButton.setFocusableInTouchMode(true);
        radioButtons.get(0).requestFocus();

    }

    private GestureDetector createGestureDetector(Context context) {
        GestureDetector gestureDetector = new GestureDetector(context);
        //Create a base listener for generic gestures
        gestureDetector.setBaseListener( new GestureDetector.BaseListener() {
            @Override
            public boolean onGesture(Gesture gesture) {
                View view = getCurrentFocus();
                if (gesture == Gesture.TAP) {
                    if(radioButtons.contains(view))
                    {
                        RadioButton tempview = (RadioButton)view;
                        tempview.setChecked(true);
                        mButton.requestFocus();
                    }
                    else view.callOnClick();
                    return true;
                } else if (gesture == Gesture.SWIPE_RIGHT) {
                    counter++;
                    radioButtons.get(counter%(radioButtons.size())).requestFocus();

                    return true;
                } else if (gesture == Gesture.SWIPE_LEFT) {
                    counter--;
                    radioButtons.get(counter%(radioButtons.size())).requestFocus();

                    return true;
                }
                return false;
            }
        });

        return gestureDetector;
    }


    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        if (mGestureDetector != null) {
            return mGestureDetector.onMotionEvent(event);
        }
        return false;
    }


    public void onClick(View v)
    {
        if(mRadioGroup.getCheckedRadioButtonId()!=-1)
        {
            int id= mRadioGroup.getCheckedRadioButtonId();
            View radioButton = findViewById(id);
            int radioId = mRadioGroup.indexOfChild(radioButton);
            RadioButton btn = (RadioButton) mRadioGroup.getChildAt(radioId);
            String selection = (String) btn.getText();
            Intent result = new Intent();
            result.putExtra("EXTRA", selection);
            setResult(1,result);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.choose, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
