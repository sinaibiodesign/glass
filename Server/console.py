from functions import connect_to_mysql, get_procedures

	
class Console:

	def __init__(self,conn,addr,storage):
		self.conn = conn
		self.addr = addr

		conn = connect_to_mysql()
		cursor = conn.cursor()

		glass_choices = storage.toString()
		procedure_names = (get_procedures(cursor))
		procedure_choices = ",".join(procedure_names)
		self.println(glass_choices+";"+procedure_choices)
		response = self.conn.recv(128)
		response = response.strip()
		response = response.split(";")
		chosen_glass = response[0].strip()
		chosen_procedure = response[1].strip()
		self.glass = storage.get(chosen_glass)
		print "console @ %s chose %s @ %s..." % (self.addr, chosen_glass, self.glass.getAddress())
		stream_link = self.glass.getStream()
		query = ("SELECT procedure_sections FROM procedures WHERE " +
				"procedure_name = '%s'" % chosen_procedure)
		cursor.execute(query, '')
		procedure_sections = cursor.next()[0]
		self.println("%s;%s" % (stream_link,procedure_sections))

	def getAddress(self):
		return self.addr

	def println(self,msg):
		self.conn.send(msg+"\r\n")

	def send_to_glass(self,msg):
		self.glass.println(msg)

	def get_glass(self):
		return self.glass