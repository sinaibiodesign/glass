class storage:

	def __init__(self):
		self.glasses = set()

	def get(self,name):
		for glass in self.glasses:
			if glass.getName() == name:
				return glass
		return None

	def contains(self,name):
		if self.get(name):
			return True
		else:
			return False

	def add(self,glass):
		if(self.contains(glass.getName())):
			return False
		self.glasses.add(glass)
		return True

	def toString(self):
		ret = ""
		for glass in self.glasses:
			ret+=glass.getName()+","
		return ret

	def remove(self,glass):
		self.glasses.remove(glass)