from glass import Glass
from console import Console
from storage import storage
from threading import Thread
from collections import OrderedDict
from functions import connect_to_mysql, get_procedures, convert_milli_to_timestamp, process_file, set_state
import mysql.connector
import logging
import socket
import time


glasses = storage()
conn = connect_to_mysql()
cursor = conn.cursor()

def main():
	print "console server has started..."

	global glasses
	global cursor

	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	server_address = ('', 9096)
	sock.bind(server_address)
	sock.listen(5)

	try:
		while True:
			conn, addr = sock.accept()
			device_type = conn.recv(128)
			handle_device_type(device_type, conn, addr)
	except:
		sock.close()

def handle_device_type(device_type, conn, addr):
	device_type = device_type.strip()
	if device_type == "GLASS":
		print "glass connected..."
		t=Thread(target=run_glass, args=(conn,addr))
		t.setDaemon(True)
		t.start()

	elif device_type == "CONSOLE":
		print "console connected..."
		t=Thread(target=run_console, args=(conn,addr))
		t.setDaemon(True)
		t.start()

def run_glass(conn,addr):
	glass = Glass(conn, addr)
	glasses.add(glass)

	while(True):
		command = conn.recv(128)
		if(command=="STOP"):
			glasses.remove(glass.getName())
			conn.close()

def run_console(conn,addr):
	console = Console(conn,addr,glasses)
	times = OrderedDict()
	current_title = "Beginning"
	start_time = "00:00:00"
	try:
		while(True):
			command = conn.recv(128).strip()
			print command
			if(command.startswith("TOGGLE")):
				console.send_to_glass(command)
				set_state(console.get_glass(),True)
			elif(command=="STOP"):
				times[start_time+";DONE"]=current_title
				conn.close()
				set_state(console.get_glass(),False)
				glasses.remove(console.get_glass())
				console.get_glass().getSocket().close()
				time.sleep(5)
				break
			elif(command.startswith("split:")):
				command = command[6:]
				end_time = convert_milli_to_timestamp(int(command.split(";")[1]))
				print current_title+start_time+"-->"+end_time
				times[start_time + ";" + end_time] = current_title
				current_title = command.split(";")[0]
				start_time = end_time

		process_file(times)
	except Exception as e:
		print e

if __name__ == "__main__": main()