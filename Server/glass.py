from functions import connect_to_mysql

class Glass:

	def __init__(self, conn, addr):
		self.conn = conn
		self.addr = addr
		print "initializing glass..."
		conn = connect_to_mysql()
		cursor = conn.cursor()
		query = "SELECT name FROM stream"
		cursor.execute(query,'')
		available_streams = []
		for (name,) in cursor:
			available_streams.append(name)
		available_streams = ",".join(available_streams)
		self.println(available_streams)
		chosen_name = self.conn.recv(128)
		chosen_name = chosen_name.strip()
		self.name = chosen_name
		print chosen_name + " has connected..."
		query = "SELECT stream,id FROM stream WHERE name = '%s'" % chosen_name
		cursor.execute(query,'')
		chosen_pair = cursor.next()
		self.stream = chosen_pair[0]
		self.id = str(chosen_pair[1])
		self.println(self.stream)
		return

	def getSocket(self):
		return self.conn

	def getStream(self):
		return self.stream

	def getName(self):
		return self.name

	def getAddress(self):
		return self.addr

	def println(self, msg):
		self.conn.send(msg+"\r\n")