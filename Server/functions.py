import mysql.connector
import re
import subprocess
import time
import os
import datetime
from converter.ffmpeg import FFMpeg

def connect_to_mysql():
	try:
		conn = mysql.connector.connect(user = 'root', database = 'glassstream')
		return conn
	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
			print("Access denied.")
		elif err.errno == errorcode.ER_BAD_DB_ERROR:
			print("Database does not exist")
		else:
			print(err)
		return None

def get_procedures(cursor):
	try:
		query = "SELECT procedure_name, procedure_sections FROM procedures"
		procedure_names = []
		cursor.execute(query,'')
		for (procedure_name, procedure_section) in cursor:
			procedure_names.append(procedure_name)
		return procedure_names
	except mysql.connector.Error as err:
		print(err)

def convert_milli_to_timestamp(ms):
	s=ms/1000 
	m,s=divmod(s,60) 
	h,m=divmod(m,60) 
	return "%02d:%02d:%02d" % (h,m,s)

def set_state(glass, state):
	conn = connect_to_mysql()
	cursor = conn.cursor()
	query = "UPDATE stream SET alive=%s WHERE id=%s" % (state, glass.id)
	cursor.execute(query,'')

def process_file(times):
	print "processing file..."
	folder_name = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S')
	os.mkdir(("/usr/local/WowzaStreamingEngine-4.2.0/content/out/%s" % (folder_name,)))
	input_file = "/usr/local/WowzaStreamingEngine-4.2.0/content/test.mp4"
	output_prefix = "/usr/local/WowzaStreamingEngine-4.2.0/content/out/%s/" % (folder_name,)
	acodec = "copy"
	vcodec = "copy"
	counter = 0
	for time_pair in times.keys():
		counter+=1
		title = times.get(time_pair)
		title = re.sub(r'[\W_]+','',title)
		output_file = "%s%d_%s.mp4" % (output_prefix,counter,title)
		begin_time = time_pair.split(";")[0]
		end_time = time_pair.split(";")[1]
		if(end_time==("DONE")):
			ss = begin_time
			subprocess.check_output(['ffmpeg','-i',input_file,'-ss',ss,'-acodec',
			acodec,'-vcodec',vcodec, '-y', output_file])
		else:
			ss = begin_time
			to = end_time
			subprocess.check_output(['ffmpeg','-i',input_file,'-ss',ss,'-to',to,'-acodec',
			acodec,'-vcodec',vcodec, '-y', output_file])

	print "done processing. files are in %s" % output_prefix