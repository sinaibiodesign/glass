package com.surgery.console;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class ChooseActivity extends Activity {

	private RadioGroup mRadioGroup;
	private RadioGroup mResolutions;
	private RadioGroup mProcedureRadioGroup;
	private int requestCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//draw layout
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose);
        //get intent information
		Intent intent = getIntent();
		requestCode = intent.getIntExtra("requestCode",0);

        //case where choosing resolution, procedure, and stream
		if(requestCode==1){

			mResolutions = (RadioGroup) findViewById(R.id.resolutions);
            mProcedureRadioGroup = (RadioGroup) findViewById(R.id.procedures);
            mRadioGroup = (RadioGroup) findViewById(R.id.radioGroup);

			String bundledProcedures = intent.getStringExtra("procedures");
            String bundledStreams = intent.getStringExtra("streams");
            String bundledResolutions = "1280x720|3000,640x360|750";

            String[] streams = bundledStreams.split(",");
            String[] procedures = bundledProcedures.split(",");
            String[] resolutions = bundledResolutions.split(",");

			Toast toast = Toast.makeText(this, procedures[0], Toast.LENGTH_LONG);
			toast.show();

            //populating stream radio group
			for(int i=0;i<procedures.length;i++)
			{
				RadioButton rdbtn = new RadioButton(this);
				rdbtn.setId(i);
				rdbtn.setText(procedures[i]);
				mProcedureRadioGroup.addView(rdbtn);
			}
            //populating procedure radio group
            for(int i=0;i<streams.length;i++)
            {
                RadioButton rdbtn = new RadioButton(this);
				rdbtn.setId(i);
				rdbtn.setText(streams[i]);
                mRadioGroup.addView(rdbtn);
            }
            //populating resolution radio group
            for(int i=0;i<resolutions.length;i++)
            {
                RadioButton rdbtn = new RadioButton(this);
				rdbtn.setId(i);
				rdbtn.setText(resolutions[i]);
                mResolutions.addView(rdbtn);
            }
		}

        //case where choosing IP Address
        else if(requestCode==2){

            mRadioGroup = (RadioGroup) findViewById(R.id.radioGroup);

            String bundledIPAddresses = intent.getStringExtra("ip_addrs");

            String[] IPAddresses = bundledIPAddresses.split(",");

            //populating IP Address radio group
            for(int i=0;i<IPAddresses.length;i++)
            {
                RadioButton rdbtn = new RadioButton(this);
				rdbtn.setId(i);
				rdbtn.setText(IPAddresses[i]);
                mRadioGroup.addView(rdbtn);
            }
        }


		

	}
	

	
	public void onClick(View okButton)
	{
        //check if an IP address has been chosen
		if(mRadioGroup.getCheckedRadioButtonId()!=-1&&requestCode==2)
		{
            //get string of IP address choice
			int radioButtonId= mRadioGroup.getCheckedRadioButtonId();
			View radioButton = findViewById(radioButtonId);
			int radioIndex = mRadioGroup.indexOfChild(radioButton);
			RadioButton btn = (RadioButton) mRadioGroup.getChildAt(radioIndex);
			String selection = (String) btn.getText();
            //send IP address choice
			Intent result = new Intent();
			result.putExtra("ip_addr", selection);
			setResult(1, result);
			finish();
		}
        //check if a procedure, stream, and resolution has been chosen
		else if(mRadioGroup.getCheckedRadioButtonId()!=-1&&mResolutions.getCheckedRadioButtonId()!=-1
				&&mProcedureRadioGroup.getCheckedRadioButtonId()!=-1&&requestCode==1)
		{
            //get string of stream choice
            int radioButtonId= mRadioGroup.getCheckedRadioButtonId();
            View radioButton = mRadioGroup.findViewById(radioButtonId);
            int radioIndex = mRadioGroup.indexOfChild(radioButton);
            RadioButton btn = (RadioButton) mRadioGroup.getChildAt(radioIndex);
            String selection = (String) btn.getText();
		    //get string of resolution choice
		    int resolutionButtonId = mResolutions.getCheckedRadioButtonId();
		    View resolutionButton = mResolutions.findViewById(resolutionButtonId);
			int resolutionIndex = mResolutions.indexOfChild(resolutionButton);
		    RadioButton btnResolution = (RadioButton) mResolutions.getChildAt(resolutionIndex);
		    String selectionResolution = (String) btnResolution.getText();
            //get string of procedure choice
			int procedureButtonId = mProcedureRadioGroup.getCheckedRadioButtonId();
			View procedureButton = mProcedureRadioGroup.findViewById(procedureButtonId);
			int procedureIndex = mProcedureRadioGroup.indexOfChild(procedureButton);
			RadioButton btnProcedure = (RadioButton) mProcedureRadioGroup.getChildAt(procedureIndex);
			String selectionProcedure = (String) btnProcedure.getText();
    	    //send procedure, stream, and resolution choices
		    Intent result = new Intent();
		    result.putExtra("stream", selection);
		    result.putExtra("resolution", selectionResolution);
			result.putExtra("procedure", selectionProcedure);
		    setResult(1, result);
		    finish();
		}

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.choose, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		return id == R.id.action_settings || super.onOptionsItemSelected(item);
	}

}
