package com.surgery.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;


import java.net.UnknownHostException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends Activity implements View.OnClickListener {

    private VideoView mVideo;
    private String res;
    private Socket socket = null;
    private PrintWriter out;
    private BufferedReader in;
    String msg;
    EditText mDuration;
    EditText mComment;
    private String ip;
    private final int PORT = 9096;
    private final int WATCH_PORT = 1935;
    private String src;
    private String procedure_splits;
    private Button mToggle;
    private boolean isStreaming = false;
    //private HashSet<Integer> split_button_ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }
        new IPChooser().execute();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_main, container,
                    false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();


        mVideo = (VideoView) findViewById(R.id.videoView);
        mToggle = (Button) findViewById(R.id.start);

    }

    private void selectIP(String str)
    {
        Intent i = new Intent(this, ChooseActivity.class);
        i.putExtra("ip_addrs", str);
        i.putExtra("requestCode", 2);
        startActivityForResult(i, 2);
    }

    private void selectStream(String streams, String procedures) {
        Intent i = new Intent(this, ChooseActivity.class);
        i.putExtra("streams", streams);
        i.putExtra("procedures", procedures);
        i.putExtra("requestCode", 1);
        startActivityForResult(i, 1);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1) {
            String stream = (data.getStringExtra("stream"));
            String procedure = (data.getStringExtra("procedure"));
            res= (data.getStringExtra("resolution"));
            Toast toast = Toast.makeText(this, stream, Toast.LENGTH_LONG);
            toast.show();
            new sendChoice().execute(stream + ";" + procedure);

        }
        else if(requestCode==2)
        {
            ip = data.getStringExtra("ip_addr");
            new ServerConnect().execute();
        }
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start:
                new sendMessage().execute("TOGGLE" + ";" + res);
                isStreaming = !isStreaming;
                if(isStreaming) {
                    mToggle.setText("Stop");
                }
                else{
                    mToggle.setText("Start");
                    new sendMessage().execute("STOP");
                }
                break;

            case R.id.play:
                Toast t = Toast.makeText(this, src, Toast.LENGTH_LONG);
                t.show();
                mVideo.setVideoURI(Uri.parse(src));
                mVideo.setMediaController(new MediaController(this));
                mVideo.requestFocus();
                mVideo.start();
                break;

            case R.id.split_buttons:
                Button b = (Button) v;
                Toast toast = Toast.makeText(this, b.getText().toString(), Toast.LENGTH_LONG);
                toast.show();
                String split = b.getText().toString();
                int time = mVideo.getCurrentPosition();
                new sendMessage().execute("split:"+split+";"+time);
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }



    private ArrayList<String> getIPs() {
        String subnet = "192.168.0";
        ArrayList<String> out = new ArrayList<String>();
        int timeout = 5;
        for (int i = 1; i < 255; i++) {
            String host = subnet + "." + i;
            try {
                if (InetAddress.getByName(host).isReachable(timeout)) {
                    out.add(host);
                }
            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return out;
    }

    private class sendChoice extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            src = "rtsp://"+ip+":1935";
            out.println(params[0]);
            String response = "";
            try {
                response = in.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            String link = response.split(";")[0];
            procedure_splits=response.split(";")[1];
            if (!link.equals("")) src += link;
            return null;
        }

        @Override
        protected void onPostExecute(Void param)
        {
            Toast toast = Toast.makeText(MainActivity.this, "Procedure Splits: " + procedure_splits + "\nSource: " + src, Toast.LENGTH_LONG);
            toast.show();
            String[] procedures =  procedure_splits.split(",");
            LinearLayout split_buttons_layout = (LinearLayout) findViewById(R.id.split_buttons_layout);
            for (String section : procedures) {
                Button btn = new Button(MainActivity.this);
                btn.setId(R.id.split_buttons);
                btn.setText(section);
                btn.setOnClickListener(MainActivity.this);
                split_buttons_layout.addView(btn);
            }
        }
    }

    private class IPChooser extends AsyncTask<Void, Void, String> {

        ProgressDialog mDialog;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();

            mDialog = ProgressDialog.show(MainActivity.this,"Loading IPs","Please wait...",true,false);
        }

        @Override
        protected String doInBackground(Void... params) {
            ArrayList<String> ips = getIPs();
            String out = "";
            for (String ipaddr : ips) {
                out += ipaddr + ",";
            }
            return out;
        }


        @Override
        protected void onPostExecute(String param) {
            selectIP(param);
            mDialog.dismiss();
        }

    }

    private class ServerConnect extends AsyncTask<Void, String, String> {

        @Override
        protected String doInBackground(Void... params) {

            //Connects to server and creates BufferedReader and PrintWriter
            if (socket == null) {
                try {
                    socket = new Socket(ip, 9096);
                    out = new PrintWriter(socket.getOutputStream(), true);
                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                } catch (UnknownHostException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            //Identifies as console to server
            out.println("CONSOLE");
            //Reads initial stream and procedure options
            try {
                String str = in.readLine();
                if (str != null) {
                    return str;
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String string) {
            //separate streams from procedures
            String streams = string.split(";")[0];
            String procedures = string.split(";")[1];
            //calls ChooseActivity with proper options
            selectStream(streams,procedures);
        }
    }

    private class sendMessage extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            out.println(params[0]);
            return null;

        }

    }
}

//private class SetupClient extends AsyncTask<Void, String, Void>
//{
//
//	@Override
//	protected Void doInBackground(Void... arg0) {
//		
//		if(socket==null){
//		try {
//			socket  = new Socket(IP, PORT);
//			out = new PrintWriter(socket.getOutputStream(),true);
//			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		}
//		
//		out.println("CONSOLE");
//	
//		String choices = "";
//		try {
//			while(true)
//			{
//			choices = in.readLine();
//			if(choices==null){}
//			else{break;}
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		publishProgress(choices);
//
//		try {
//			Thread.sleep(4000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		out.println(choice);
//		
//		return null;
//	}
//	
//	@Override
//	protected void onProgressUpdate(String... strings)
//	{
//		choice = getChoice();
//		Toast toast = Toast.makeText(MainActivity.this, strings[0], Toast.LENGTH_LONG);
//		toast.show();
//	}
//	
//	
//}


//private class SendMessage extends AsyncTask<String, Void, Void>
//{
//
//	@Override
//	protected Void doInBackground(String... arg0) {
//		out.println(arg0[0]);
//		return null;
//	}
//	
//	}
//	

	

